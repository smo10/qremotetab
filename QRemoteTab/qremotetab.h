#ifndef QREMOTETAB_H
#define QREMOTETAB_H

#include <QtGui/QMainWindow>

class QAction;
class QActionGroup;
class QMenu;
class QStatusBar;

class QVBoxLayout;
class QPlainTextEdit;

class TabletCanvas;
class TabletSenderConnection;
class QTabletData;


class QRemoteTab : public QMainWindow
{
   Q_OBJECT

public:
   QRemoteTab(TabletCanvas* canvas, QPlainTextEdit* debugLog, TabletSenderConnection* connection, QWidget *parent = 0, Qt::WFlags flags = 0);
   ~QRemoteTab();

private slots:
   void aboutAct();
   void toggleDebug(bool enabled);
   void sendData(const QTabletData& data);
   void toggleFullscreen(bool fullscreen);

private:
   void createActions();
   void createMenus();

   TabletCanvas* _tabletCanvas;
   TabletSenderConnection* _connection;

   QVBoxLayout* vLayout;

   QPlainTextEdit* _debugLog;

   QAction* debugAction;

   QAction *exitAction;

   QAction *fullscreenAction;

   QAction *aboutAction;
   QAction *aboutQtAction;

   QMenu *fileMenu;
   QMenu *optionsMenu;
   QMenu *helpMenu;
};

#endif // QREMOTETAB_H
