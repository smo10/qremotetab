#ifndef TABLETSENDERCONNECTION_H
#define TABLETSENDERCONNECTION_H

#include <QObject>
#include <QAbstractSocket>
#include <QUdpSocket>


class TabletSenderConnection : public QObject
{

   Q_OBJECT;

public:
   TabletSenderConnection(const QString& hostname, const quint16 port);
   ~TabletSenderConnection();

   void connectToHost();

   void send(const QByteArray& data);

   bool isConnected() { return _connected; }

public slots:
   void handleConnected();
   void handleDisconnected();
   void handleError(QAbstractSocket::SocketError err);

signals:
   void error(int socketError, const QString &message);
   void connected();
   void disconnected();

private:
   bool _connected;

   QUdpSocket socket;
   QString _hostname;
   quint16 _port;
};


#endif

