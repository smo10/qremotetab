#include "connectdialog.h"

#include "qtabletappmanager.h"
#include "tabletsenderconnection.h"

#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QMessageBox>

ConnectDialog::ConnectDialog(QWidget* parent)
   : QDialog(parent), _connection(NULL)
{
   layout = new QGridLayout(this);

   quint32 row = 0;

   QLabel* hostLabel = new QLabel(tr("Hostname:"), this);
   hostEdit = new QLineEdit(this);
   hostLabel->setBuddy(hostEdit);
   layout->addWidget(hostLabel, row, 0);
   layout->addWidget(hostEdit, row, 1);
   ++row;

   QLabel* portLabel = new QLabel(tr("Port:"), this);
   portEdit = new QSpinBox(this);
   portLabel->setBuddy(portEdit);
   portEdit->setRange(1000, 65535);
   layout->addWidget(portLabel, row, 0);
   layout->addWidget(portEdit, row, 1);
   ++row;

   connectButton = new QPushButton(tr("Connect"), this);
   exitButton = new QPushButton(tr("Exit"), this);
   layout->addWidget(connectButton, row, 0);
   layout->addWidget(exitButton, row, 1);
   ++row;

   connect(exitButton, SIGNAL(clicked()), this, SLOT(exitButtonClick()));
   connect(connectButton, SIGNAL(clicked()), this, SLOT(connectButtonClick()));
   
   setWindowTitle(tr("Connect to Host"));
}

ConnectDialog::~ConnectDialog()
{
   if (NULL != _connection)
   {
      delete _connection; _connection = NULL;
   }
}

void ConnectDialog::exitButtonClick()
{
   this->close();
}

void ConnectDialog::connectButtonClick()
{
   if (hostEdit->text().isEmpty())
   {
      QMessageBox badnews(QMessageBox::Critical, tr("Error"), tr("Please enter a host name!"), QMessageBox::Ok, this);
      badnews.exec();
      return;
   }

   toggleFields(false);

   if (NULL == _connection)
   {
      _connection = new TabletSenderConnection(hostEdit->text(), portEdit->value());
   }

   connect(_connection, SIGNAL(connected()), this, SLOT(handleConnected()));
   connect(_connection, SIGNAL(error(int, QString)), this, SLOT(handleError(int, QString)));

   _connection->connectToHost();
}

TabletSenderConnection* ConnectDialog::takeConnection()
{
   TabletSenderConnection* temp = _connection;
   _connection = NULL;
   return temp;
}

void ConnectDialog::handleConnected()
{
   emit connected();
}

void ConnectDialog::handleError( int socketError, const QString &message )
{
   toggleFields(true);

   QMessageBox badnews(QMessageBox::Critical, tr("Error"), message, QMessageBox::Ok, this);
   badnews.exec();
}

void ConnectDialog::toggleFields( bool enabled )
{
   hostEdit->setEnabled(enabled);
   portEdit->setEnabled(enabled);
   connectButton->setEnabled(enabled);
   exitButton->setEnabled(enabled);
}

