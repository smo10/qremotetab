#include "tabletsenderconnection.h"

#include <QtNetwork>


TabletSenderConnection::~TabletSenderConnection()
{
   socket.close();
}

TabletSenderConnection::TabletSenderConnection( const QString& hostname, const quint16 port )
   : _connected(false), _hostname(hostname), _port(port)
{
   connect(&socket, SIGNAL(connected()), this, SLOT(handleConnected()));
   connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(handleError(QAbstractSocket::SocketError)));
   connect(&socket, SIGNAL(disconnected()), this, SLOT(handleDisconnected()));
}

void TabletSenderConnection::connectToHost()
{
   socket.connectToHost(_hostname, _port, QIODevice::ReadWrite);
}

void TabletSenderConnection::send(const QByteArray& data)
{
   if (_connected)
   {
       socket.write(data);
   }
}

void TabletSenderConnection::handleConnected()
{
   _connected = true;
   emit connected();
}

void TabletSenderConnection::handleDisconnected()
{
   _connected = false;
   emit disconnected();
}

void TabletSenderConnection::handleError( QAbstractSocket::SocketError err )
{
   emit error(socket.error(), socket.errorString());
}



