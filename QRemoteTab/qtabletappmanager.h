#ifndef QTABLETAPPMANAGER_H
#define QTABLETAPPMANAGER_H

#include <QObject>

class ConnectDialog;
class QRemoteTab;

class TabletCanvas;
class QPlainTextEdit;

class QTabletAppManager : public QObject
{
   Q_OBJECT

public:
   QTabletAppManager(TabletCanvas* canvas, QPlainTextEdit* debugger);
   ~QTabletAppManager();

public slots:
   void openApp();

private:
   ConnectDialog* connection;
   QRemoteTab* app;

   TabletCanvas* _canvas;     // Not owned, but may need to delete
   QPlainTextEdit* _debugger; // Not owned, but may need to delete

   bool _appOpened;
};


#endif

