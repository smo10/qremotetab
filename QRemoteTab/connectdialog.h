#ifndef CONNECTDIALOG_H
#define CONNECTDIALOG_H

#include <QDialog>

class QGridLayout;
class QLineEdit;
class QSpinBox;
class QTabletAppManager;
class QPushButton;
class TabletSenderConnection;

class ConnectDialog : public QDialog
{
   Q_OBJECT

public:
   ConnectDialog(QWidget* parent = 0);
   ~ConnectDialog();

   TabletSenderConnection* takeConnection();

private slots:
   void exitButtonClick();
   void connectButtonClick();
   void handleConnected();
   void handleError(int socketError, const QString &message);

signals:
   void connected();

private:
   void toggleFields(bool enabled);

   QGridLayout* layout;

   QLineEdit* hostEdit;
   QSpinBox* portEdit;
   QPushButton* connectButton;
   QPushButton* exitButton;

   TabletSenderConnection* _connection;
};



#endif

