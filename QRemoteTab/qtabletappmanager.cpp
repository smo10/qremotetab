#include "qtabletappmanager.h"
#include "connectdialog.h"
#include "qremotetab.h"


QTabletAppManager::QTabletAppManager(TabletCanvas* canvas, QPlainTextEdit* debugger)
   : _canvas(canvas), _debugger(debugger), _appOpened(false)
{

   connection = new ConnectDialog();
   connection->show();
   
   connect(connection, SIGNAL(connected()), this, SLOT(openApp()));
}

QTabletAppManager::~QTabletAppManager()
{
   if (!_appOpened)
   {
      if (NULL != _canvas)
      {
         delete _canvas; _canvas = NULL;
      }

      if (NULL != _debugger)
      {
         delete _debugger; _debugger = NULL;
      }
   }
}

void QTabletAppManager::openApp()
{
   connection->close();

   app = new QRemoteTab(_canvas, _debugger, connection->takeConnection());
   app->show();

   _appOpened = true;
}
