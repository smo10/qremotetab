#include "qtabletappmanager.h"
#include "tabletapplication.h"
#include "tabletcanvas.h"

#include <QPlainTextEdit>

QPlainTextEdit* debugger = NULL;

void debugMessage(QtMsgType type, const char *msg)
{
   if (NULL != debugger)
   {
      debugger->appendPlainText(QString(msg));
   }
}

int main(int argc, char *argv[])
{
    TabletApplication a(argc, argv);
    TabletCanvas *canvas = new TabletCanvas;
    a.setCanvas(canvas);

    debugger = new QPlainTextEdit;
    debugger->setReadOnly(true);
    debugger->setMaximumHeight(100);
    debugger->setVisible(false);
    qInstallMsgHandler(debugMessage);

    QTabletAppManager appManager(canvas, debugger);

    return a.exec();
}

