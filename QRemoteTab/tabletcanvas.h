#ifndef TABLETCANVAS_H
#define TABLETCANVAS_H

#include <QWidget>
#include <QPixmap>
#include <QPoint>
#include <QTabletEvent>

class QPaintEvent;
class QString;

class QTabletData;

class TabletCanvas : public QWidget
{
   Q_OBJECT

public:
   TabletCanvas();

   void setTabletDevice(QTabletEvent::TabletDevice device)
   {
      tabletDevice = device;
   }

signals:
   void dataReady(const QTabletData& data);

protected:
   void tabletEvent(QTabletEvent *event);
   void paintEvent(QPaintEvent *event);
   void resizeEvent(QResizeEvent *event);

private:
   void initPixmap();

   QTabletEvent::PointerType pointerType;
   QTabletEvent::TabletDevice tabletDevice;

   QPixmap pixmap;
};

#endif
