#include "qremotetab.h"

#include "tabletcanvas.h"
#include "tabletsenderconnection.h"
#include <qtabletdata.h>
#include <version.h>

// GUI window
#include <QMessageBox>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QApplication>

// GUI components
#include <QPlainTextEdit>
#include <QVBoxLayout>


QRemoteTab::QRemoteTab(TabletCanvas* canvas, QPlainTextEdit* debugLog, TabletSenderConnection* connection, QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags), _tabletCanvas(canvas), _debugLog(debugLog), _connection(connection)
{
   createActions();
   createMenus();

   QWidget* container = new QWidget(this);

   vLayout = new QVBoxLayout(container);
   vLayout->setMargin(0);

   vLayout->addWidget(_tabletCanvas);

   if (NULL != _debugLog)
   {
      vLayout->addWidget(_debugLog);
   }

   //setLayout(vLayout);

   setCentralWidget(container);
   setWindowTitle(tr("QRemoteTab"));

   connect(_tabletCanvas, SIGNAL(dataReady(QTabletData)), this, SLOT(sendData(QTabletData)));
}

QRemoteTab::~QRemoteTab()
{
   if (NULL != _connection)
   {
      delete _connection; _connection = NULL;
   }

   if (NULL != _tabletCanvas)
   {
      delete _tabletCanvas; _tabletCanvas = NULL;
   }

   if (NULL != _debugLog)
   {
      delete _debugLog; _debugLog = NULL;
   }
}


void QRemoteTab::aboutAct()
{
   ABOUT_DIALOG
}

void QRemoteTab::createActions()
{
   if (NULL != _debugLog)
   {
      debugAction = new QAction(tr("Show Debug &Log"), this);
      debugAction->setShortcut(tr("Ctrl+D"));
      debugAction->setCheckable(true);
      debugAction->setChecked(_debugLog->isVisible());
      connect(debugAction, SIGNAL(triggered(bool)), this, SLOT(toggleDebug(bool)));
   }

   exitAction = new QAction(tr("E&xit"), this);
   exitAction->setShortcuts(QKeySequence::Quit);
   connect(exitAction, SIGNAL(triggered()),
      this, SLOT(close()));

   fullscreenAction = new QAction(tr("Fullsc&reen"), this);
   fullscreenAction->setShortcut(tr("Ctrl+F"));
   fullscreenAction->setCheckable(true);
   fullscreenAction->setChecked(isFullScreen());
   connect(fullscreenAction, SIGNAL(triggered(bool)), this, SLOT(toggleFullscreen(bool)));

   aboutAction = new QAction(tr("A&bout"), this);
   aboutAction->setShortcut(tr("Ctrl+B"));
   connect(aboutAction, SIGNAL(triggered()),
      this, SLOT(aboutAct()));

   aboutQtAction = new QAction(tr("About &Qt"), this);
   aboutQtAction->setShortcut(tr("Ctrl+Q"));
   connect(aboutQtAction, SIGNAL(triggered()),
      qApp, SLOT(aboutQt()));
}

void QRemoteTab::createMenus()
{
   fileMenu = menuBar()->addMenu(tr("&File"));

   if (NULL != _debugLog)
   {
      fileMenu->addAction(debugAction);
      addAction(debugAction);
   }

   fileMenu->addSeparator();
   fileMenu->addAction(exitAction);
   

   optionsMenu = menuBar()->addMenu(tr("&Options"));
   optionsMenu->addAction(fullscreenAction);
   addAction(fullscreenAction);  // Need to add it to the main as well or else it gets disabled when the menubar is hidden

   helpMenu = menuBar()->addMenu("&Help");
   helpMenu->addAction(aboutAction);
   helpMenu->addAction(aboutQtAction);
}

void QRemoteTab::toggleDebug( bool enabled )
{
   if (NULL != _debugLog)
   {
      _debugLog->setVisible(enabled);
   }
}

void QRemoteTab::sendData( const QTabletData& data )
{
   QByteArray serialized;

   QDataStream datastr(&serialized, QIODevice::ReadWrite);
   datastr << data;

   _connection->send(serialized);
}

void QRemoteTab::toggleFullscreen(bool fullscreen)
{
   if (!fullscreen)
   {
      showNormal();
      menuBar()->show();
   }
   else
   {
      showFullScreen();
      menuBar()->hide();
   }
}


