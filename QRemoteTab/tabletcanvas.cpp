#include "tabletcanvas.h"

#include <QtGui>
#include  <qtabletdata.h>

TabletCanvas::TabletCanvas()
{
   setMinimumSize(500, 500);
   setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
   initPixmap();
   setAutoFillBackground(true);
   tabletDevice = QTabletEvent::Stylus;
}

void TabletCanvas::initPixmap()
{
   QPixmap newPixmap = QPixmap(width(), height());
   newPixmap.fill(Qt::white);
   QPainter painter(&newPixmap);
   if (!pixmap.isNull())
      painter.drawPixmap(0, 0, pixmap);
   painter.end();
   pixmap = newPixmap;
}


void TabletCanvas::tabletEvent(QTabletEvent *event)
{
   QPoint relPos(event->pos().x() * 65535 / width(), event->pos().y() * 65535 / height());

   switch (event->type()) {
   case QEvent::TabletPress:
      {
	      QTabletData data(QTabletData::TabletPress, relPos);
	      emit dataReady(data);
      }
      break;
   case QEvent::TabletRelease:
      {
         QTabletData data(QTabletData::TabletRelease, relPos);
         emit dataReady(data);
      }
      break;
   case QEvent::TabletMove:
      {
         QTabletData data(QTabletData::TabletMove, relPos);
         emit dataReady(data);
      }
      break;
   default:
      break;
   }
}

void TabletCanvas::paintEvent(QPaintEvent *)
{
   QPainter painter(this);
   painter.drawPixmap(0, 0, pixmap);
}

void TabletCanvas::resizeEvent(QResizeEvent *event)
{
   initPixmap();
}

