#include "qtabletdata.h"

#include <QDataStream>

QTabletData::~QTabletData()
{
}

QTabletData::QTabletData( Type t, const QPoint& thepos )
   : _type(t), _pos(thepos)
{
}

QTabletData::QTabletData()
{
}

QDataStream & operator<<( QDataStream &s, const QTabletData &d )
{
   s << (quint8) d.type();
   s << d.pos();
   return s;
}

QDataStream & operator>>( QDataStream &s, QTabletData &d )
{
   quint8 type;
   s >> type;
   d.setType(static_cast<QTabletData::Type>(type));

   s >> const_cast<QPoint&>(d.pos());

   return s;
}
