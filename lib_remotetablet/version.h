#ifndef VERSION_H
#define VERSION_H

#define APP_VERSION "0.1"

#define ABOUT_DIALOG QMessageBox::about(this, tr("QRemoteTab"), \
   tr("By Simon Mo\n\nVersion: %1\nCompiled: %2 %3").arg(APP_VERSION, __DATE__, __TIME__));

#endif
