#ifndef QTABLETDATA_H
#define QTABLETDATA_H

#include <QPointF>

class QTabletData
{

public:
   enum Type {
      TabletPress = 0,
      TabletRelease = 1,
      TabletMove = 2
   };

   QTabletData();
   QTabletData(Type t, const QPoint& thepos);
   ~QTabletData();

   inline Type type() const { return static_cast<Type>(_type); }
   inline const QPoint& pos() const { return _pos; }

   inline void setType(Type t) { _type = t; }

private:
   quint8 _type;
   QPoint _pos;
};

#ifndef QT_NO_DATASTREAM
QDataStream &operator<<(QDataStream &, const QTabletData &);
QDataStream &operator>>(QDataStream &, QTabletData &);
#endif

#endif
