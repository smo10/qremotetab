#ifndef CONNECTDIALOG_H
#define CONNECTDIALOG_H

#include <QDialog>

class QSpinBox;
class QPushButton;

class ConnectDialog : public QDialog
{
   Q_OBJECT;

public:
   ConnectDialog(QWidget* parent = 0);

   quint16 getPort();

private slots:
   void exitButtonClick();
   void connectButtonClick();

private:
   QSpinBox* portEdit;

   QPushButton* connectButton;
   QPushButton* exitButton;

};

#endif
