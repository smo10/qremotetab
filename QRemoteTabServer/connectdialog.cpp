#include "connectdialog.h"

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>

ConnectDialog::ConnectDialog( QWidget* parent )
   : QDialog(parent)
{
   QGridLayout* layout = new QGridLayout(this);
   
   int row = 0;

   QLabel* portLabel = new QLabel(tr("Port:"), this);
   portEdit = new QSpinBox(this);
   portLabel->setBuddy(portEdit);
   portEdit->setRange(1000, 65535);
   layout->addWidget(portLabel, row, 0);
   layout->addWidget(portEdit, row, 1);
   ++row;

   connectButton = new QPushButton(tr("OK"), this);
   exitButton = new QPushButton(tr("Exit"), this);
   layout->addWidget(connectButton, row, 0);
   layout->addWidget(exitButton, row, 1);
   ++row;

   connect(exitButton, SIGNAL(clicked()), this, SLOT(exitButtonClick()));
   connect(connectButton, SIGNAL(clicked()), this, SLOT(connectButtonClick()));

   setWindowTitle(tr("Enter Port to Listen"));
}

void ConnectDialog::connectButtonClick()
{
   done(QDialog::Accepted);
}

void ConnectDialog::exitButtonClick()
{
   done(QDialog::Rejected);
}

quint16 ConnectDialog::getPort()
{
   return (quint16) portEdit->value();
}



