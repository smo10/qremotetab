#ifndef QREMOTETABSERVER_H
#define QREMOTETABSERVER_H

#include <QtGui/QMainWindow>
#include <QtNetwork>

class QPlainTextEdit;
class QCursor;
class QTabletData;

class QRemoteTabServer : public QMainWindow
{
    Q_OBJECT

public:
    QRemoteTabServer(const quint16 port, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~QRemoteTabServer();

    QPlainTextEdit* getDebugger();

public slots:
   void readData();
   void processData(const QTabletData& data);

signals:
   void acquiredData(const QTabletData& data);

private slots:
   void aboutAct();

private:
   void createActions();
   void createMenus();

   quint16 _port;

   QUdpSocket* socket;

   QPlainTextEdit* _debugger;

   QAction *exitAction;

   QAction *aboutAction;
   QAction *aboutQtAction;

   QMenu *fileMenu;
   QMenu *helpMenu;
};

#endif // QREMOTETABSERVER_H
