#include "qremotetabserver.h"
#include <QtGui/QApplication>

#include "connectdialog.h"

#include <QPlainTextEdit>

QPlainTextEdit* debugger = NULL;

void debugMessage(QtMsgType type, const char *msg)
{
   if (NULL != debugger)
   {
      debugger->appendPlainText(QString(msg));
   }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ConnectDialog newConn;
    int result = newConn.exec();

    if (QDialog::Rejected == result)
    {
       return 0;
    }

    QRemoteTabServer w(newConn.getPort());
    debugger = w.getDebugger();
    qInstallMsgHandler(debugMessage);

    w.showMinimized();
    return a.exec();
}
