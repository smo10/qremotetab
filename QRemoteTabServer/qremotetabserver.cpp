#include "qremotetabserver.h"

#include <qtabletdata.h>
#include <version.h>

// Mouse simulation includes
#ifdef WIN32
#include <windows.h>
#endif

// GUI window
#include <QMessageBox>
#include <QAction>
#include <QMenu>
#include <QMenuBar>

// GUI components
#include <QVBoxLayout>
#include <QPlainTextEdit>
#include <QStatusBar>
#include <QCursor>

// Networking
#include <QHostAddress>
#include <QByteArray>
#include <QDataStream>


QRemoteTabServer::QRemoteTabServer(const quint16 port, QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags), _port(port)
{
   createActions();
   createMenus();

   QWidget* container = new QWidget(this);

   QVBoxLayout* layout = new QVBoxLayout(container);

   _debugger = new QPlainTextEdit(this);
   _debugger->setReadOnly(true);
   _debugger->setMaximumHeight(100);

   layout->addWidget(_debugger);

   container->setLayout(layout);

   setCentralWidget(container);
   setWindowTitle(tr("QRemoteTab Server"));

   connect(this, SIGNAL(acquiredData(QTabletData)), this, SLOT(processData(QTabletData)));

   socket = new QUdpSocket(this);
   socket->bind(_port, QUdpSocket::DontShareAddress);
   connect(socket, SIGNAL(readyRead()), this, SLOT(readData()));

   statusBar()->showMessage(tr("Waiting for data..."));
}

QRemoteTabServer::~QRemoteTabServer()
{
   socket->close();
}

void QRemoteTabServer::readData()
{
   quint64 len = 0;
   quint32 processed = 0;

   while (socket->hasPendingDatagrams())
   {
      QByteArray data;
      data.resize(socket->pendingDatagramSize());
      socket->readDatagram(data.data(), data.size());

      len += data.size();

      QDataStream datastr(data);

      while (!datastr.atEnd())
      {
         ++processed;
         QTabletData tabData;
         datastr >> tabData;
         emit acquiredData(tabData);
      }
   }

   //quint64 len = client->bytesAvailable();
   //QByteArray data = client->read(len);
   
   //qDebug() << "Packet len: " << len << "; Packets Processed: " << processed;
}

QPlainTextEdit* QRemoteTabServer::getDebugger()
{
   return _debugger;
}

void QRemoteTabServer::processData( const QTabletData& data )
{
   // Windows only code here for now
#ifdef WIN32
   MOUSEINPUT mInput = {0};
   mInput.dx = data.pos().x();
   mInput.dy = data.pos().y();

   mInput.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_VIRTUALDESK;

   switch (data.type())
   {
   case QTabletData::TabletPress:
      {
         mInput.dwFlags |= MOUSEEVENTF_LEFTDOWN;
      }
      break;
   case QTabletData::TabletRelease:
      {
         mInput.dwFlags |= MOUSEEVENTF_LEFTUP;
      }
      break;
   case QTabletData::TabletMove:
      {
         mInput.dwFlags |= MOUSEEVENTF_MOVE;
      }
      break;
   default:
      break;
   }

   INPUT input = {0};

   input.type = INPUT_MOUSE;
   input.mi = mInput;

   SendInput(1, &input, sizeof(input));

   qDebug() << "x: " << mInput.dx << "; y: " << mInput.dy;
#endif
}

void QRemoteTabServer::createActions()
{
   exitAction = new QAction(tr("E&xit"), this);
   exitAction->setShortcuts(QKeySequence::Quit);
   connect(exitAction, SIGNAL(triggered()),
      this, SLOT(close()));

   aboutAction = new QAction(tr("A&bout"), this);
   aboutAction->setShortcut(tr("Ctrl+B"));
   connect(aboutAction, SIGNAL(triggered()),
      this, SLOT(aboutAct()));

   aboutQtAction = new QAction(tr("About &Qt"), this);
   aboutQtAction->setShortcut(tr("Ctrl+Q"));
   connect(aboutQtAction, SIGNAL(triggered()),
      qApp, SLOT(aboutQt()));
}

void QRemoteTabServer::createMenus()
{
   fileMenu = menuBar()->addMenu(tr("&File"));
   fileMenu->addSeparator();
   fileMenu->addAction(exitAction);

   helpMenu = menuBar()->addMenu("&Help");
   helpMenu->addAction(aboutAction);
   helpMenu->addAction(aboutQtAction);
}

void QRemoteTabServer::aboutAct()
{
   ABOUT_DIALOG
}

